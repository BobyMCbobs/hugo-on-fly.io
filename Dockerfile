FROM registry.gitlab.com/bobymcbobs/go-http-server@sha256:d485653d4ba54f166c785a90526fe9ab71734c58d1acb2175c9f7b472feb9276
ENV APP_SERVE_FOLDER=/var/go-http-server/www
COPY public /var/go-http-server/www
