#+title: Hugo on Fly.io

#+begin_quote
Deploy a static Hugo site onto Fly.io
#+end_quote

* Introduction

This is an expandable proof of concept repo, which aims to showcase deploying a Hugo built site to Fly.io.

This guide presumes that you have an existing git repo, with a Hugo site as the contents.

* Preparations

The following CLI utilities are required
- [[https://fly.io/docs/hands-on/install-flyctl][flyctl]]

Log into your Fly.io account, [[https://fly.io/docs/hands-on/sign-in][see this]].

Copy the following files into your repo
#+begin_src bash
cp -r hack/deploy.sh Dockerfile fly.toml .deploy.env \
    ~/MYREPOFOLDER
#+end_src

Update the .deploy.env to match your site
#+begin_src bash
SITE_URL="https://mycoolsitereally.com/"
FLY_APP=MYCOOLFLYAPPNAME
FLY_REGIONS=(
    fra
    sjc
    syd
)
#+end_src

* Customisation

See:
- Fly App configuration: [[https://fly.io/docs/reference/configuration][https://fly.io/docs/reference/configuration]]
- Go-HTTP-Server configuration: [[https://bobymcbobs.gitlab.io/go-http-server/configuration][https://bobymcbobs.gitlab.io/go-http-server/configuration]]
- [[./Dockerfile][Dockerfile]]
- [[./hack/deploy.sh][hack/deploy.sh]] can be customised to add more providers, instead of (just) Fly.io
- manage regions, as mapped in the [[./.deploy.env][.deploy.env]], [[https://fly.io/docs/reference/regions][see fly docs here]]

* Deploying

Initially launch a Fly app, [[https://fly.io/docs/reference/fly-launch][see here]], a command similar to:
#+begin_src bash
flyctl launch --copy-config --no-deploy
#+end_src

Deploy
#+begin_src bash
./hack/deploy.sh
#+end_src

Please note it may take a short amount of time until the site is first accessible.

Allocate an IP address, [[https://fly.io/docs/flyctl/ips][see here]], a command similar to:
#+begin_src bash
flyctl ips allocate-v4
#+end_src

Assigning a custom domain and TLS cert, [[https://fly.io/docs/flyctl/certs][see here]], a command similar to:
#+begin_src bash
flyctl certs create example.com
#+end_src

Ensure to follow the instructions regarding DNS configuration for your chosen domain from the output of the above command.

* Maintainance

Ensure that the base image of go-http-server is up-to-date, only using pinned tags (for example: ~registry.gitlab.com/bobymcbobs/go-http-server@sha256:d485653d4ba54f166c785a90526fe9ab71734c58d1acb2175c9f7b472feb9276~), [[https://gitlab.com/BobyMCbobs/go-http-server][see here]].
See the logs for the site, [[https://fly.io/docs/getting-started/working-with-fly-apps/#viewing-logs][see here]].

* Auto deploy through CI

It is possible to auto deploy using CI such as GitLab CI and GitHub Actions.
For GitLab CI, checkout the example in [[./.gitlab-ci.example.yml][.gitlab-ci.example.yml]], and [[https://docs.gitlab.com/ee/ci/variables/index.html#define-a-cicd-variable-in-the-ui][docs]] for setting secrets.
For GitHub Actions, checkout the example in [[./.github/workflows/deploy.yml][.github/workflows/deploy.yml]], and [[https://docs.github.com/en/actions/security-guides/encrypted-secrets][docs]] for setting secrets.

In each CI, a secret for ~FLY_API_SECRET~ must be set with the value of a generated API secret.
Read more about API secrets, [[https://fly.io/docs/app-guides/continuous-deployment-with-github-actions/#api-tokens-and-deployment][here]].

This is not set by default.

* Notices
- read the docs!
- you are responsible for costs incurred
- customise all the things
- most of all: have fun

* License

This repo is licensed under the MIT license and comes with absolutely no warranty.
