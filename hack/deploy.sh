#!/bin/bash

cd "$(git rev-parse --show-toplevel)"

date

DEPLOY_TO=fly
DOCKERFILE=./Dockerfile

ARGS=("${@}")
case "${ARGS[0]}" in
    debug)
        set -x
        ;;
    exit)
        exit 0
        ;;
esac

function fly_reconcile_app_regions {
    REGIONS=("${@}")
    FLY_REGIONS=($(flyctl regions list --app "${FLY_APP}" --json | jq -r .Regions[].Code | sort | xargs))
    HAS_CHANGE=false
    for REGION in "${REGIONS[@]}"; do
        if echo "${FLY_REGIONS[@]}" | grep -q "${REGION}"; then
            continue
        fi
        flyctl regions add "${REGION}" --app "${FLY_APP}"
        HAS_CHANGE=true
    done
    for FLY_REGION in "${FLY_REGIONS[@]}"; do
        if echo "${REGIONS[@]}" | grep -q "${FLY_REGION}"; then
            continue
        fi
        flyctl regions remove "${FLY_REGION}" --app "${FLY_APP}"
        HAS_CHANGE=true
    done
    if [ "${HAS_CHANGE}" = true ]; then
        echo "Using the following regions: "${REGIONS[*]}""
    fi
}

function fly_set_app_scale {
    AMOUNT="$1"
    FLY_SCALE="$(flyctl scale show --app calebwoodbine-com --json | jq -r .Count)"
    if [ "${AMOUNT}" = "${FLY_SCALE}" ]; then
        return
    fi
    flyctl scale count "${AMOUNT}" --max-per-region 1 --app "${FLY_APP}"
    echo "Using scale of ${AMOUNT}"
}

function fly_build_container {
    # NOTE, if there's a main.go and go-http-server is being imported manually
    #       for a custom backend, then ko may be used to help with this
    if [ ! -f main.go ]; then
        return
    fi
    export KO_DOCKER_REPO="registry.fly.io/${FLY_APP}"
    IMAGE="$(ko publish --push --jobs 100 --bare --platform linux/amd64 .)"
    cat <<EOF >"${DOCKERFILE}"
FROM $IMAGE
EOF
    echo "Built and pushed container image"
}

function fly_deploy {
    flyctl deploy --app "${FLY_APP}" --dockerfile "${DOCKERFILE}"
}

function hugo_build {
    hugo \
        --baseURL "${SITE_URL}" \
        --minify
    echo "Built Hugo site"
}

function config_print {
    (
        echo "FIELD : VALUE"
        echo ". . ."
        echo "SITE_URL : $SITE_URL"
        echo "FLY_APP : $FLY_APP"
        echo "FLY_REGIONS : $(echo ${FLY_REGIONS[@]} | sed 's/ /+/g')"
    ) | column -t
}

function deploy {
    time (
        case "$DEPLOY_TO" in
            fly)
                fly_reconcile_app_regions "${FLY_REGIONS[@]}"
                fly_set_app_scale "${#FLY_REGIONS[@]}"
                hugo_build
                fly_build_container
                fly_deploy
                ;;
            *)
                echo "NO PROVIDER SELECTED" >/dev/stderr
                exit 1
                ;;
        esac
    )
}

. .deploy.env
config_print
deploy
